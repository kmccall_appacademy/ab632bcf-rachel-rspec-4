class Book
  def title=(value)
    @title = value
  end

  def title
    exceptions = ["and", "in", "the", "of", "a", "an"]
    words = @title.split
    words.map! do |el|
      if exceptions.include?(el)
        el
      else
        el.capitalize
      end
    end
    words[0] = words[0].capitalize
    words.join(" ")
  end

end
