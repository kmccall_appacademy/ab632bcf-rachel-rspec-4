class Temperature
  def initialize(opts = {})
    @hsh = opts
  end

  def in_celsius
    if @hsh.has_key?(:c)
      return @hsh[:c]
    else
      x = @hsh[:f].to_f
      (((x - 32)*5)/9)
    end
  end

  def in_fahrenheit
    if @hsh.has_key?(:f)
      return @hsh[:f]
    else
      x = @hsh[:c].to_f
      ((x * 9)/5) + 32
    end
  end

  def self.from_celsius(num)
  	 return self.new(:c => num)
   end

   def self.from_fahrenheit(num)
  	 return self.new(:f => num)
   end
end

class Celsius < Temperature
  def initialize(num)
    @hsh = {:c => num}
  end
end

class Fahrenheit < Temperature
  def initialize(num)
    @hsh = {:f => num}
  end
end
