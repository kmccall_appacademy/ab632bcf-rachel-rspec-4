class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    formatted = {hours: (@seconds / 60) / 60, minutes: (@seconds / 60) % 60, seconds: @seconds % 60}
    string_format = formatted.map do |k, v|
      if v < 10
        "0#{v}"
      else
        v.to_s
      end
    end
    string_format.join(":")
  end

end



# describe "Timer" do
#   before(:each) do
#     @timer = Timer.new
#   end
#
#   it "should initialize to 0 seconds" do
#     expect(@timer.seconds).to eq(0)
#   end
#
#   describe "time_string" do
#     it "should display 0 seconds as 00:00:00" do
#       @timer.seconds = 0
#       expect(@timer.time_string).to eq("00:00:00")
#     end



# class Book
#   def title=(value)
#     @title = value
#   end
#
#   def title
#     exceptions = ["and", "in", "the", "of", "a", "an"]
#     words = @title.split
#     words.map! do |el|
#       if exceptions.include?(el)
#         el
#       else
#         el.capitalize
#       end
#     end
#     words[0] = words[0].capitalize
#     words.join(" ")
#   end
#
# end
