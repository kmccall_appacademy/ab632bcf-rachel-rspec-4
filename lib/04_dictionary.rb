class Dictionary

  attr_accessor :entries
  attr_accessor :keywords

  def entries
    @dictionary
  end

  def initialize
    @dictionary = {}
    @ks = @dictionary.keys
    @vs = @dictionary.values
  end

  def add(entry)
    if entry.is_a? String
      @dictionary[entry] = nil
    else
      @dictionary = @dictionary.merge(entry)
    end
  end

  def include? (word)
    @dictionary.keys.include?(word)
  end

  def keywords
     words = @dictionary.keys
     key_arr = words.sort
   end

   def match(chunk)
     length = chunk.length
     self[0..length] == chunk
   end

   def find(something)
     if @dictionary == {}
       return {}
    else
      keys_list = @dictionary.keys
      matches = keys_list.select { |el| el.match(something) }
      result_hash = {}
      matches.each { |el| result_hash[el] = @dictionary[el] }
      result_hash
    end
   end

   def printable
     arr = []
     keywords.each do |el|
       arr << %Q([#{el}] "#{@dictionary[el]}")
     end
     arr.join("\n")
   end

end
